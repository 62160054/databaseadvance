/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.storeproject.poc;

import database.Database;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Memos
 */
public class TestConnection {
    public static void main(String[] args) {
        Connection conn = null;
        Database db = Database.getInstance();
        db.close();
        try{
            if(conn != null){
                conn.close();
            }
        } catch (SQLException ex){
            System.out.println("Error : Can't close Database");
        }
    }
}
